FROM ruby:2.4.3-alpine

ENV LANG ja_JP.UTF-8
ENV RAILS_ENV production
ENV SECRET_KEY_BASE 58d811c9d3c18864231bde25116d7bf2c7588998a3e48a0d421b60fff92ce6211e2c502992e309ec21e0cd28d9b3feba6b7dfb9d0f2409a0b06c9d25fe120e35

RUN apk add --update\
    bash \
    build-base \
    curl-dev \
    git \
    libxml2-dev \
    libxslt-dev \
    linux-headers \
    nodejs \
    openssh \
    ruby-dev \
    ruby-json \
    tzdata \
    yaml \
    yaml-dev \
    zlib-dev \
    sqlite-dev \
    && rm -rf /var/cache/apk/*

RUN gem install bundler
RUN npm install -g yarn

WORKDIR /tmp
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install --path vendor/bundle --without test development

ENV APP_HOME /myapp
RUN mkdir -p $APP_HOME
WORKDIR $APP_HOME
COPY . $APP_HOME

RUN rake assets:precompile
RUN ln -sf /dev/stdout log/production.log
