class AddPublishedToShows < ActiveRecord::Migration[5.1]
  def change
    add_column :shows, :published, :boolean, default: false, null: false
  end
end
