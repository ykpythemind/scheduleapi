class ChangeShowsConstraint < ActiveRecord::Migration[5.1]
  def change
    remove_index :shows, :user_id
    add_index :shows, :user_id
  end
end
