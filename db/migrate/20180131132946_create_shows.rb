class CreateShows < ActiveRecord::Migration[5.1]
  def change
    create_table :shows do |t|
      t.text :body
      t.string :title
      t.date :day
      t.string :img
      t.string :place
      t.integer :user_id, null: false

      t.timestamps
    end
    add_index :shows, :user_id, unique: true
  end
end
