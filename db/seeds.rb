# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.find_or_create_by!(name: ENV.fetch('FIRST_USER_NAME') { 'user' }) do |user|
  user.password = ENV.fetch('FIRST_USER_PASS') { '123456' }
end
