json.extract! show, :id, :body, :title, :day, :img, :place, :user_id, :created_at, :updated_at
json.url show_url(show, format: :json)
