class ImageUploadsController < ApplicationController
  before_action :set_user, :set_show
  before_action :basic_auth

  def edit
    if params[:preview]
      url = url_for(@show.image) if @show.image.attached?
      render :json, url
    else
      render
    end
  end

  def create
    @show.image.attach(params.require(:image))
    flash[:notice] = '画像を保存しました'
    redirect_to user_shows_path(@user)
  end

  def destroy
    @show.image.purge_later
    flash[:notice] = '画像を削除しました'
    redirect_to user_shows_path(@user)
  end

  private

  def set_show
    @show = Show.find(params[:show_id])
  end
end
