class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def set_user
    @user = User.find_by(name: params[:user_id]) || User.find(params[:user_id])
  end

  def basic_auth
    authenticate_or_request_with_http_basic do |user, pass|
      user == @user.name && @user.authenticate(pass)
    end
  end

end
