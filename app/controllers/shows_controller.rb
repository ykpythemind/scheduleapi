class ShowsController < ApplicationController
  before_action :set_user
  before_action :basic_auth, except: [:index, :show] # TODO: remove later
  before_action :set_show, only: [:show, :edit, :update, :destroy]

  protect_from_forgery except: :index # TODO: think

  # GET /shows
  # GET /shows.json
  def index
    @shows = Show.where(user: @user).order(day: :desc).all
    respond_to do |format|
      format.html { render :index }
      format.json { render json: @shows.map(&:json_obj), callback: callback_param }
    end
  end

  # GET /shows/1
  # GET /shows/1.json
  def show
    respond_to do |format|
      format.html { render :show }
      format.json { render json: @show.json_obj }
    end
  end

  # GET /shows/new
  def new
    @show = @user.shows.build
  end

  # GET /shows/1/edit
  def edit
  end

  # POST /shows
  # POST /shows.json
  def create
    @show = Show.new(show_params)

    respond_to do |format|
      if @show.save
        format.html { redirect_to user_shows_path(@user), notice: 'Show was successfully created.' }
        format.json { render :show, status: :created, location: @show }
      else
        format.html { render :new }
        format.json { render json: @show.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shows/1
  # PATCH/PUT /shows/1.json
  def update
    respond_to do |format|
      if @show.update(show_params)
        format.html { redirect_to user_shows_path(@user), notice: 'Show was successfully updated.' }
        format.json { render :show, status: :ok, location: @show }
      else
        format.html { render :edit }
        format.json { render json: @show.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shows/1
  # DELETE /shows/1.json
  def destroy
    @show.destroy
    respond_to do |format|
      format.html { redirect_to user_shows_path(@user), notice: 'Show was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_show
      @show = Show.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def show_params
      params.require(:show).permit(:body, :title, :day, :img, :place, :user_id, :published, :image)
    end

    def callback_param
      params[:callback].present? ? params[:callback].gsub(/\W/, '') : nil
    end
end
