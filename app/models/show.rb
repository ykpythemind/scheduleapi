class Show < ApplicationRecord
  belongs_to :user
  has_one_attached :image
  scope :publish, -> { where(published: true) }

  def json_obj
    {
      title: title,
      body: body,
      day: I18n.l(day),
      place: place,
      img: img
    }
  end
end
